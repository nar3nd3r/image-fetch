from shutil import copyfileobj, rmtree
import requests
from argparse import ArgumentParser as Parser
from os.path import exists, basename
from os import mkdir
import logging
from re import sub
logging.basicConfig(format='%(asctime)s........\t%(message)s',)
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def get_image_url(filename):
    with open(filename, 'r') as file_reader:
        for url in file_reader:
            yield sub('\n', '', url)


def main(filename=None):
    if exists(filename):
        logger.info(f"Found {filename}")
        try:
            line = get_image_url(filename)
            while True:
                new_url = next(line)
                logger.info(new_url)
                try:
                    headers = {
                        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
                    response = requests.get(
                        new_url, headers=headers, stream=True)
                    if response.status_code == 200:
                        if not exists('./images'):
                            mkdir(path='./images')
                        output = f"./images/{basename(new_url)}"
                        if exists(output):
                            shutil.rmtree(output)
                        with open(output, 'wb') as out_file:
                            copyfileobj(response.raw, out_file)
                        del response, output
                    else:
                        logger.warning(str(response))
                except Exception as e:
                    logger.warning(str(e))
        except StopIteration:
            # suppress silently (end of iterator), no yelling
            logger.info("Done!")
    else:
        logger.error(f"Unable to locate {filename}")


if __name__ == '__main__':
    parser = Parser(description='Process url file for fetching images.')
    parser.add_argument(
        '--filename', help='path to local text file containing image url')
    args = parser.parse_args()
    if args.filename:
        main(filename=args.filename)
    else:
        parser.print_help()
